#!/usr/bin/env python
# -*- coding: utf-8 -*-

from xml.etree import ElementTree
from geojson import Feature, Point, FeatureCollection, dumps
import sys


cod_postais = [("","", "")]
ruas = [("","")]
features = []
coord = []
newcoord = [0.0,0.0]
city = ""
piso = ""

if len(sys.argv) != 3:
	raise ValueError('Este script precisa de dois argumentos, ./script.py in.gml out.geojson')

#abrir ficheiro especificado no primeiro argumento 

with open(sys.argv[1], 'rt') as f:
	tree = ElementTree.parse(f)

#obter codigos postais e respetivos ids
for membro in tree.findall('{http://www.opengis.net/wfs/2.0}member'):
	postal = membro.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}PostalDescriptor')
	for nome in postal:
		id = nome.get("{http://www.opengis.net/gml/3.2}id")
		cod = nome.find('{http://inspire.ec.europa.eu/schemas/ad/4.0}postCode').text
		if nome[3][0][5][0][0].text is not None:
			city = nome[3][0][5][0][0].text.title()
		cod_postais.append(("#" + id, cod, city))


#obter lista com ruas e respetivos ids
for membro in tree.findall('{http://www.opengis.net/wfs/2.0}member'):
	for endereco in membro.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}ThoroughfareName'):
		if endereco[3][0][0][0][5][0][0].text is not None:
			end_nome = endereco[3][0][0][0][5][0][0].text.title()
		else:
			end_nome = ""
#			print(endereco[3][0][0][0][5][0][0].text.title())
		id = endereco.get("{http://www.opengis.net/gml/3.2}id")
		ruas.append(("#" + id, end_nome))



for membro in tree.findall('{http://www.opengis.net/wfs/2.0}member'):
	featproperties = {}
	addr = membro.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}Address')
	for locator in addr:

		#Obter coordenadas
		for pos in locator.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}position'):
			for geopos in pos.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}GeographicPosition'):
				for geo in geopos.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}geometry'):
					for point in geo.findall('{http://www.opengis.net/gml/3.2}Point'):
						for pos in point.findall('{http://www.opengis.net/gml/3.2}pos'):
							i = 1
							
							#Inverter os campos das coordenadas
							for coord in pos.text.split(" "):
								newcoord[i] = float(coord)
								i=i-1


		#obter id node (possivelmente não será usado)
		id = locator.get("{http://www.opengis.net/gml/3.2}id")

		#obter id rua
		#rua = locator.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}component')[0].get('{http://www.w3.org/1999/xlink}href')

		#obter id codigo postal
		#cod_postal = locator.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}component')[1].get('{http://www.w3.org/1999/xlink}href')

		for components in locator.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}component'):
			split = components.get('{http://www.w3.org/1999/xlink}href').split(".")
			if split[3] == "TN": #Rua
				rua = components.get('{http://www.w3.org/1999/xlink}href')
			if split[3] == "PD": #Cod Postal
				cod_postal = components.get('{http://www.w3.org/1999/xlink}href')
				
		#obter numero policia
		for loc in locator.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}locator'):
			for addrlocator in loc.findall('{http://inspire.ec.europa.eu/schemas/ad/4.0}AddressLocator'):
				for desig in addrlocator:
					try:
						if desig[0][1].get('{http://www.w3.org/1999/xlink}href') == "http://inspire.ec.europa.eu/codelist/LocatorDesignatorTypeValue/buildingIdentifier":
							numporta = desig[0][0].text
						if desig[0][1].get('{http://www.w3.org/1999/xlink}href') == "http://inspire.ec.europa.eu/codelist/LocatorDesignatorTypeValue/floorIdentifier":
							piso = desig[0][0].text
					except IndexError:
						pass
					#numporta = addrlocator[2][0][0].text		#[0][0] é o piso e [1][0] é o lado (ESQ, DIR, FRT, etc)
					#piso = addrlocator[0][0][0].text
									
		
		#fazer substituição do ID pelo cod postal
		for cp in cod_postais:
			if (cod_postal == cp[0]):
				cod_postal = cp[1]
				city = cp[2]

		#fazer substituição do ID pela rua
		for r in ruas:
			if (rua == r[0]):
				rua = r[1]
		
		
		#Obter campo "text" (onde alguns endereços tem o seu numero de policia registado)
		note = locator[3][0][3][0][0][0][5][0][0].text
		if note is not None and note != "":
			featproperties['note'] = note
#

		#Compilar tudo para poder exportar o ponto
		if piso is not None:
			piso = ''.join(filter(str.isdigit, piso))
			if piso != '':
				featproperties['building:levels'] = piso
		if city is not None:
			featproperties['addr:city'] = city
		if cod_postal is not None:
			featproperties['addr:postcode'] = cod_postal
		if rua is not None:
			featproperties['addr:street'] = rua
		if numporta is not None:
			featproperties['addr:housenumber'] = numporta
		
		features.append(Feature(geometry=Point(newcoord), properties=featproperties))
		#features.append(Feature(geometry=Point(newcoord), properties={"addr:housenumber": numporta, "addr:postcode": cod_postal, "addr:street": rua, "addr:city": city, "note": note, "building:levels": piso}))
		#print(dumps(features))

#Criar coleção de "pontos
fc = FeatureCollection(features)

#abrir ficheiro de saida como escrita
text_file = open(sys.argv[2], "w")

#escrever no ficheiro 
n = text_file.write(dumps(fc, sort_keys=True))

#fechar o ficheiro
text_file.close()

