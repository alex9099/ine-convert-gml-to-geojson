#!/usr/bin/env python
# -*- coding: utf-8 -*-

from xml.etree import ElementTree
from geojson import Feature, Point, FeatureCollection, dumps, loads as loadgeo, utils
import sys

newfeatures = []
newfeatures = FeatureCollection(newfeatures)
if len(sys.argv) != 3:
    raise ValueError('Este script precisa de dois argumentos, ./script.py in.geojson out.geojson')

#abre ficheiro definido no primeiro argumento
with open(sys.argv[1], 'rt') as f:
	feature = loadgeo(f.read())

#precorre as features de entrada
for feat in feature['features']:
	old_geo = feat['geometry']
	f=0
	s=0
	#Precorre as novas features
	for newfeat in newfeatures['features']:
		#Se a feature que estamos a tentar adicionar já tiver na array de novas features...
		if old_geo == newfeat['geometry']:
			f = 1
			if feat['properties'].get('building:levels') is not None and newfeat['properties'].get('building:levels') is not None:
				if int(feat['properties'].get('building:levels')) > int(newfeat['properties'].get('building:levels')):
					s = 1
					newfeat['properties']['building:levels'] = feat['properties']['building:levels']
					break
			elif newfeat['properties'].get('building:levels') is None and feat['properties'].get('building:levels') is not None:
				newfeat['properties']['building:levels'] = feat['properties']['building:levels']
			
		#if old_geo == newfeat['geometry']:
			#definir f=1...
			#f = 1
			#break
#		pass
	#Se f se manter igual (=0) adicionar a feature na nova array
	if f == 0 and s == 0:
		newfeatures['features'].append(feat)


#abre ficheiro definido no segundo argumento em modo escrita
text_file = open(sys.argv[2], "w")

#escreve no ficheiro 
n = text_file.write(dumps(newfeatures, sort_keys=True))
text_file.close()
